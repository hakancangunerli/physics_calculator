import math
import sympy
from kinematics import *
# there's no switch in python, so we'll use if statements


# Dynamics and Kinematic
# Gravitation
# Curving Motion
# Work/Energy Problems
# Angular Momentum

def main():
    choice = str(input(" Dynamics and Kinematic (k), Gravitation (g) , Curving Motion(m) ,Work/Energy Problems(w), Angular Momentum(a)? "))
    # we can use pattern matching but then they'd need 3.10 or above to use it, so we'll use if statements
    choice = choice[0].lower()
    if choice == 'k':
        kinematics()
    if choice == 'g':
        gravitation()
    if choice == 'm':
        curve_motion()
    if choice == 'w':
        work_energy()
    if choice == 'a':
        angular_momentum()
    else:
        print("for now, this is all we can calculate, sorry!")


if __name__ == "__main__":
    main()
