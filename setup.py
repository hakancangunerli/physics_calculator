from setuptools import setup


setup(name='PhysicsProject',
      version='0.0.1',
      description='simple tool for calculating stuff',
      author='cna',
      url='',
      install_requires=['matplotlib==3.3.3',
                        'mpmath==1.1.0',
                        'numpy==1.19.4',
                        'sympy==1.7.1',
                        ])
