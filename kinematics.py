# d= vi*t + 1/2*a*t^2
# vf = vi + at
# vf^2 = vi^2 + 2ad
# d= (vi + vf)/2 * t
def kinematics():
    choice = str(input("Calculate distance or velocity? (d/v): "))
    if choice == 'd':
        missing = str(
            input("what is missing? (acceleration[a] ,final velocity[v])? "))
        if missing == 'a':
            distance_without_acceleration()
        if missing == 'v':
            # this would also be negative in cases where we have initial velo instead of final velo
            distance_without_final_velocity()
        else:
            print("Invalid input")
    if choice == 'v':
        missing = str(input("what is missing? (distance[d] , time[t]? "))
        if missing == 'd':
            velocity_without_distance()
        if missing == 't':
            velocity_without_time()
        else:
            print("Invalid input")


def distance_without_final_velocity():
    vi = float(input("Enter initial velocity: "))
    a = float(input("Enter acceleration: "))
    t = float(input("Enter time: "))
    d = vi*t + 0.5*a*t**2
    print("The distance traveled is: ", d)


def distance_without_acceleration():
    vi = float(input("Enter initial velocity: "))
    vf = float(input("Enter final velocity: "))
    t = float(input("Enter time: "))
    d = (vi + vf)/2 * t
    print("The distance traveled is: ", d)


def velocity_without_distance():
    vi = float(input("Enter initial velocity: "))
    a = float(input("Enter acceleration: "))
    t = float(input("Enter time: "))
    vf = vi + a*t
    print("The final velocity is: ", vf)


def velocity_without_time():
    vi = float(input("Enter initial velocity: "))
    a = float(input("Enter acceleration: "))
    d = float(input("Enter distance: "))
    # this might have some issues since it might fail at negatives.
    vf = math.sqrt(vi**2 + 2*d*a)
    print("The final velocity is: ", vf)
